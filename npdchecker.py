import httpx
from typing import Union
from datetime import datetime
import asyncio


class NPDError(Exception):
	def __init__(self, response_json: dict):
		self.code = response_json["code"]
		self.message = response_json["message"]
		super().__init__(self.__str__())

	def __str__(self):
		return f"<{self.code}> {self.message}"

	def __repr__(self):
		return self.__str__()


class NPDChecker:
	def __call__(self, inn: Union[int, str], date: str = None):
		return self.is_npd(inn, date)

	def __init__(self):
		self.api_url = "https://statusnpd.nalog.ru:443/api/v1/tracker/taxpayer_status"
		self.client = httpx.Client()
		self.a_client = None
		self.is_npd = self.check
		self.a = self.a_check

	def check(self, inn: Union[int, str], date: str = None):
		date = date if date else datetime.now().strftime("%Y-%m-%d")
		inn = str(inn)
		r = self.client.post(self.api_url, json={"inn": inn, "requestDate": date})
		if "status" in r.json():
			return r.json()["status"]
		else:
			raise NPDError(r.json())

	async def a_check(self, inn: Union[int, str], date: str = None):
		if not self.a_client:
			self.a_client = httpx.AsyncClient()
		date = date if date else datetime.now().strftime("%Y-%m-%d")
		inn = str(inn)
		r = await self.a_client.post(self.api_url, json={"inn": inn, "requestDate": date})
		json = r.json()
		if "status" in json:
			return json["status"]
		else:
			raise NPDError(r.json())

	async def __aenter__(self):
		return self

	async def __aexit__(self, *args):
		return await self.a_client.aclose()

	def run(self, coroutine):
		loop = asyncio.get_event_loop()
		run = loop.run_until_complete
		if coroutine:
			run(coroutine)


